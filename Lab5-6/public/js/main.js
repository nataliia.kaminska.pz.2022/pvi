const chatForm = document.getElementById("chat-form");
const chatMessages = document.getElementById("chat-messages");
const userList = document.getElementById("users");

const { username } = Qs.parse(location.search, {
  ignoreQueryPrefix: true,
});

const socket = io();

let privateRecipient = null;
let privateRecipientName = null;

// Join chatroom
socket.emit("joinChat", { username });

// Get users
socket.on("users", (users) => {
  outputUsers(users);
});

// Message from server
socket.on("message", (message) => {
  outputMessage(message);
  chatMessages.scrollTop = chatMessages.scrollHeight;
});

// Private message from server
socket.on("privateMessage", (message) => {
  outputPrivateMessage(message);
  chatMessages.scrollTop = chatMessages.scrollHeight;
});

// Message submit
chatForm.addEventListener("submit", (e) => {
  e.preventDefault();

  const msg = e.target.elements.msg.value;

  if (!privateRecipient) {
    alert("Please select a user to chat with.");
    return;
  }

  if (privateRecipient === socket.id) {
    alert("You cannot send messages to yourself.");
    return;
  }

  socket.emit("privateChatMessage", { to: privateRecipient, msg });

  e.target.elements.msg.value = "";
  e.target.elements.msg.focus();
});

// Output message to DOM
function outputMessage(message) {
  const div = document.createElement("div");
  div.classList.add("message");
  div.innerHTML = `<p class="meta">${message.username} <span>${message.time}</span></p>
  <p class="text">
    ${message.text}
  </p>`;
  chatMessages.appendChild(div);
}

// Output private message to DOM
function outputPrivateMessage(message) {
  const div = document.createElement("div");
  div.classList.add("message", "private");
  div.innerHTML = `<p class="meta">${message.username} (private) <span>${message.time}</span></p>
  <p class="text">
    ${message.text}
  </p>`;
  chatMessages.appendChild(div);
}

// Add users to DOM
function outputUsers(users) {
  userList.innerHTML = "";
  users.forEach((user) => {
    if (user.id !== socket.id) {
      const li = document.createElement("li");
      li.innerText = user.username;
      li.dataset.id = user.id;
      li.addEventListener("click", () => {
        privateRecipient = user.id;
        privateRecipientName = user.username;
        chatMessages.innerHTML = `<h2>Private chat with ${user.username}</h2>`;
      });
      userList.appendChild(li);
    }
  });
}
