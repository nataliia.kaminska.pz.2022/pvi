$(document).ready(function () {
  const addStudentModalWrapper = $("#add-student");
  const confirmDeletionModalWrapper = $("#confirm-deletion");
  const addStudentModal = addStudentModalWrapper.find(".modal");
  const confirmDeletionModal = confirmDeletionModalWrapper.find(".modal");

  const addStudentForm = $("#add-student-form");
  const addStudentButton = $("#add-student-btn");
  const closeModalButton = $("#close-modal-btn");
  const studentsTable = $("#students-table");

  addStudentButton.on("click", showStudentModal);
  closeModalButton.on("click", hideStudentModal);

  $(document).on("mousedown", function (e) {
    handleClickOutside(e, addStudentModal, hideStudentModal);
    handleClickOutside(e, confirmDeletionModal, hideConfirmationModal);
  });

  function onStudentEdit(e, id) {
    e.preventDefault();
    const formData = new FormData(e.target);
    const student = { ...Object.fromEntries(formData), id };
    sendRequestToServer("edit", student);
  }

  function onStudentCreate(e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    const student = Object.fromEntries(formData);
    sendRequestToServer("add", student);
  }

  function onDeleteConfirmation(id) {
    sendRequestToServer("delete", { id });
  }

  function sendRequestToServer(action, data) {
    console.log("Data sent to server:", data);
    return (
      fetch("http://localhost/forpvi/validate_student.php", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ action, ...data }),
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error("Network response was not ok.");
          }
          return response.json();
        })
        //старе
        .then((jsonData) => {
          if (jsonData.error) {
            for (const errorField in jsonData.errors) {
              console.error(`${errorField}: ${jsonData.errors[errorField]}`);
            }
            return false;
          } else {
            console.log("Server honest reaction:", jsonData);
            clearTable();
            getStudentsFromDatabase();
            return true;
          }
        })
        //нове
        /*.then((jsonData) => {
          if (jsonData.error) {
            console.error(jsonData.error);
            alert("Something went wrong. Check console for more info.");
          } else {
            switch (action) {
              case "add":
                createStudent(jsonData, false);
                break;
              case "edit":
                createStudent(jsonData, true);
                break;
              case "delete":
                removeStudent(data.id);
                break;
              default:
                console.error("Invalid action");
            }
            hideStudentModal();
          }
        })*/
        .catch((error) => {
          console.error("Error sending data to server:", error);
          return false;
        })
    );
  }
  /*function sendRequestToServer(action, data) {
    console.log("Data sent to server:", data);
    fetch("http://localhost/forpvi/validate_student.php", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ action, ...data }),
    })
      .then((response) => response.json())
      .then((jsonData) => {
        if (jsonData.error) {
          console.error(jsonData.error);
          alert("Something went wrong. Check console for more info.");
        } else {
          switch (action) {
            case "add":
              createStudent(jsonData, false);
              break;
            case "edit":
              createStudent(jsonData, true);
              break;
            case "delete":
              removeStudent(data.id);
              break;
            default:
              console.error("Invalid action");
          }
          hideStudentModal();
        }
      })
      .catch((error) => {
        console.error("Error sending data to server:", error);
        alert("Something went wrong. Check console for more info.");
      });
  }*/

  function createStudent(
    { id, group_name, name, surname, birthday, gender, active },
    replace
  ) {
    const tr = $("<tr>").attr("id", `student-${id}`);
    const checkbox = $("<input>").attr("type", "checkbox");
    const checkBoxTd = $("<td>").append(checkbox);
    tr.append(checkBoxTd);

    const cells = [group_name, `${name} ${surname}`, gender, birthday];

    for (let cellContent of cells) {
      const td = $("<td>").text(cellContent);
      tr.append(td);
    }

    const activeTd = $("<td>");
    const activeDiv = $("<div>").addClass("status");
    active && activeDiv.addClass("active");
    activeTd.append(activeDiv);
    tr.append(activeTd);

    const optionsTd = $("<td>");
    const editButton = $("<button>");
    const removeButton = $("<button>");
    editButton.on("click", onEditButtonClick(id));
    removeButton.on("click", () => confirmDeletion(id));

    const editButtonIcon = $("<i>")
      .addClass("fa fa-edit")
      .css("color", "white");
    const removeButtonIcon = $("<i>")
      .addClass("fa fa-remove")
      .css("color", "white");

    editButton.append(editButtonIcon);
    removeButton.append(removeButtonIcon);

    optionsTd.append(editButton, removeButton);
    tr.append(optionsTd);

    if (replace) {
      $(`#student-${id}`).replaceWith(tr);
    } else {
      studentsTable.find("tbody").append(tr);
    }
    return tr;
  }

  function confirmDeletion(id) {
    showConfirmationModal();
    const confirmDeletionButton = $("#confirm-deletion-btn");
    const cancelDeletionButton = $("#cancel-deletion-btn");
    //const student = students.find((el) => el.id === id);
    $("#confirm-question").text(`Are you sure you want to delete student?`);

    confirmDeletionButton.on("click", () => {
      onDeleteConfirmation(id);
      hideConfirmationModal();
    });
    cancelDeletionButton.on("click", hideConfirmationModal);
  }

  function removeStudent(id) {
    $(`#student-${id}`).remove();
  }

  function hideStudentModal() {
    addStudentModalWrapper.addClass("hidden");
  }

  function showStudentModal(mode, id) {
    if (mode === "edit") {
      $("#modal-title").text("Edit student");

      // Отримання даних студента з поточного рядка таблиці
      const group_name = $(`#student-${id}`).find("td:nth-child(2)").text();
      const name = $(`#student-${id}`)
        .find("td:nth-child(3)")
        .text()
        .split(" ")[0];
      const surname = $(`#student-${id}`)
        .find("td:nth-child(3)")
        .text()
        .split(" ")[1];
      const gender = $(`#student-${id}`).find("td:nth-child(4)").text();
      const birthday = $(`#student-${id}`).find("td:nth-child(5)").text();

      // Заповнення форми редагування даними студента
      $("#group_name").val(group_name);
      $("#name").val(name);
      $("#surname").val(surname);
      $("#birthday").val(birthday);
      $(`input[name='gender'][value='${gender}']`).prop("checked", true);

      // Встановлення обробника події на форму редагування
      addStudentForm.off("submit").on("submit", (e) => {
        onStudentEdit(e, id);
      });
    } else {
      $("#modal-title").text("Create student");
      addStudentForm.off("submit").on("submit", onStudentCreate);
    }
    addStudentModalWrapper.removeClass("hidden");
  }

  // Оновлення id при кліку на кнопку редагування студента
  function onEditButtonClick(id) {
    return function () {
      showStudentModal("edit", id);
    };
  }

  // Додайте обробник події для кнопки редагування

  function hideConfirmationModal() {
    confirmDeletionModalWrapper.addClass("hidden");
  }

  function showConfirmationModal() {
    confirmDeletionModalWrapper.removeClass("hidden");
  }

  function handleClickOutside({ target }, element, callback) {
    if (element && !element.is(target) && element.has(target).length === 0) {
      callback();
    }
  }

  // Функція для отримання студентів з бази даних і їх відображення на екрані
  function getStudentsFromDatabase() {
    fetch("http://localhost/forpvi/get_students.php")
      .then((response) => response.json())
      .then((data) => {
        if (data.error) {
          console.error("Error:", data.error);
        } else {
          data.forEach((student) => {
            createStudent(student, false);
          });
        }
      })
      .catch((error) => console.error("Error fetching students:", error));
  }
  function clearTable() {
    studentsTable.find("tbody").empty();
  }
  // Викликаємо функцію для отримання студентів після завантаження сторінки
  getStudentsFromDatabase();
});
