<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: http://127.0.0.1:5500');
header('Access-Control-Allow-Methods: POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');

// Підключення до бази даних MySQL
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "students";

$conn = new mysqli($servername, $username, $password, $dbname);

// Перевірка з'єднання
if ($conn->connect_error) {
    die(json_encode(array("error" => "Error: " . $sql . " " . $conn->error)));
}

// Додавання студента
function addStudent($group_name, $name, $surname, $birthday, $gender) {
    global $conn;
    // Виконання запиту до бази даних
    $sql = "INSERT INTO students (group_name, name, surname, birthday, gender) VALUES ('$group_name', '$name', '$surname', '$birthday', '$gender')";
    if ($conn->query($sql) === TRUE) {
        return json_encode(array("success" => true));
    } else {
        return json_encode(array("error" => "Error: " . $sql . " " . $conn->error));
    }
}

// Редагування студента
function editStudent($id, $group_name, $name, $surname, $birthday, $gender) {
    global $conn;
    // Виконання запиту до бази даних
    $sql = "UPDATE students SET group_name='$group_name', name='$name', surname='$surname', birthday='$birthday', gender='$gender' WHERE id=$id";
    if ($conn->query($sql) === TRUE) {
        return json_encode(array("success" => true));
    } else {
        return json_encode(array("error" => "Error: " . $sql . " " . $conn->error));
    }
}

// Видалення студента
function deleteStudent($id) {
    global $conn;

    // Виконання запиту до бази даних
    $sql = "DELETE FROM students WHERE id=$id";
    if ($conn->query($sql) === TRUE) {
        return json_encode(array("success" => true));
    } else {
        return json_encode(array("error" => "Error: " . $sql . " " . $conn->error));
    }
}

// Обробка запиту
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $data = json_decode(file_get_contents("php://input"), true);
    $group_name = $data['group_name'] ?? '';
    $name = $data['name'] ?? '';
    $gender = $data['gender'] ?? '';
    $birthday = $data['birthday'] ?? '';

    if ($data['action'] === 'delete') {
        echo deleteStudent($data['id']);
        return; // Вийти, щоб уникнути виконання подальшого коду
    }
    else{
    $errors = [];
    //if (empty($data['group_name'])) {
    //    $errors['group_name'] = "Group must be selected.";
    //}
    if (empty($data['name']) || !preg_match('/^[A-Za-z]+$/', $data['name'])) {
        $errors['name'] = "Name must contain only letters and not be empty.";
    }
    if (empty($data['surname']) || !preg_match('/^[A-Za-z]+$/', $data['name'])) {
        $errors['surname'] = "Surname must contain only letters and not be empty.";
    }
    if (empty($data['birthday']) || !preg_match('/^\d{4}-\d{2}-\d{2}$/', $data['birthday'])) {
        $errors['birthday'] = "Please enter a valid birthday (yyyy-mm-dd).";
    } else {
        $parts = explode('-', $data['birthday']);
        $year = intval($parts[0]);
        $month = intval($parts[1]);
        $day = intval($parts[2]);
        if ($month < 1 || $month > 12 || $day < 1 || $day > 31 || $year < 1934 || $year > 2014) {
            $errors['birthday'] = "Please enter a valid birthday (yyyy-mm-dd, between 1934 and 2014).";
        }
    }

    if (!empty($errors)) {
        echo json_encode(['error' => true, 'message' => 'Validation failed.', 'errors' => $errors]);
        //http_response_code(400);
        exit;
    } else {
    if(isset($data['action'])) {
        switch ($data['action']) {
            case 'add':
                echo addStudent($data['group_name'], $data['name'], $data['surname'], $data['birthday'], $data['gender']);
                break;
            case 'edit':
                echo editStudent($data['id'], $data['group_name'], $data['name'], $data['surname'], $data['birthday'], $data['gender']);
                break;
            //case 'delete':
            //    echo deleteStudent($data['id']);
            //    break;
            default:
                echo json_encode(array("error" => "Invalid action"));
        }
    } else {
        echo json_encode(array("error" => "No action provided"));
    }
    }
    }
}

// Закриття з'єднання з базою даних
$conn->close();
?>
