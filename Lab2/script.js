$(document).ready(function () {
  const addStudentModalWrapper = $("#add-student");
  const confirmDeletionModalWrapper = $("#confirm-deletion");
  const addStudentModal = addStudentModalWrapper.find(".modal");
  const confirmDeletionModal = confirmDeletionModalWrapper.find(".modal");

  const addStudentForm = $("#add-student-form");
  const addStudentButton = $("#add-student-btn");
  const closeModalButton = $("#close-modal-btn");
  const studentsTable = $("#students-table");

  const students = [
    {
      id: 0,
      group: "PZ-22",
      name: "Konstantyn",
      surname: "Vykhovanets",
      birthday: "2005-12-27",
      gender: "F",
      active: false,
    },
    {
      id: 1,
      group: "PZ-22",
      name: "Denys",
      surname: "Hudzenko",
      birthday: "2005-01-22",
      gender: "M",
      active: false,
    },
  ];

  students.forEach((el) => createStudent(el));

  addStudentButton.on("click", showStudentModal);
  closeModalButton.on("click", hideStudentModal);

  $(document).on("mousedown", function (e) {
    handleClickOutside(e, addStudentModal, hideStudentModal);
    handleClickOutside(e, confirmDeletionModal, hideConfirmationModal);
  });

  function onStudentEdit(e, id) {
    const formData = new FormData(e.target);
    let isValid = true;
    const errorMessages = {};

    const group = formData.get("group");
    if (!group) {
      isValid = false;
      errorMessages.group = "Group must be selected.";
    }

    const name = formData.get("name").trim();
    if (!name || !/^[A-Za-z]+$/.test(name)) {
      isValid = false;
      errorMessages.name = "Name must contain only letters.";
    }

    const surname = formData.get("surname").trim();
    if (!surname || !/^[A-Za-z]+$/.test(surname)) {
      isValid = false;
      errorMessages.surname = "Surname must contain only letters.";
    }

    const birthday = formData.get("birthday");
    if (!birthday) {
      isValid = false;
      errorMessages.birthday = "Please enter a valid birthday.";
    } else {
      const parts = birthday.split("-");
      if (parts.length !== 3) {
        isValid = false;
        errorMessages.birthday =
          "Please enter the date in the format dd.mm.yyyy.";
      } else {
        const year = parseInt(parts[0], 10);
        const month = parseInt(parts[1], 10);
        const day = parseInt(parts[2], 10);
        if (
          isNaN(year) ||
          isNaN(month) ||
          isNaN(day) ||
          month < 1 ||
          month > 12 ||
          day < 1 ||
          day > 31 ||
          year < 1934 ||
          year > 2014
        ) {
          isValid = false;
          errorMessages.birthday =
            "Please enter a valid birthday (yyyy-mm-dd, between 1934 and 2014).";
        }
      }
    }

    if (!isValid) {
      $(".error-message").remove();
      $(".error-field").removeClass("error-field");

      for (const field in errorMessages) {
        const inputField = $("#" + field);
        const errorDiv = $("<div>")
          .addClass("error-message")
          .text(errorMessages[field]);
        inputField.parent().append(errorDiv);
      }

      return;
    }
    $(".error-message").remove();
    $(".error-field").removeClass("error-field");
    const student = { ...Object.fromEntries(formData), id };
    students[students.findIndex((el) => el.id === id)] = student;
    createStudent(student, true);
    hideStudentModal();
    sendStudentDataToServer(student);
  }

  function onStudentCreate(e) {
    e.preventDefault();
    const formData = new FormData(e.target);

    let isValid = true;
    const errorMessages = {};

    const group = formData.get("group");
    if (!group) {
      isValid = false;
      errorMessages.group = "Group must be selected.";
    }

    const name = formData.get("name").trim();
    if (!name || !/^[A-Za-z]+$/.test(name)) {
      isValid = false;
      errorMessages.name = "Name must contain only letters.";
    }

    const surname = formData.get("surname").trim();
    if (!surname || !/^[A-Za-z]+$/.test(surname)) {
      isValid = false;
      errorMessages.surname = "Surname must contain only letters.";
    }

    const birthday = formData.get("birthday");
    if (!birthday) {
      isValid = false;
      errorMessages.birthday = "Please enter a valid birthday.";
    } else {
      const parts = birthday.split("-");
      if (parts.length !== 3) {
        isValid = false;
        errorMessages.birthday =
          "Please enter the date in the format dd.mm.yyyy.";
      } else {
        const year = parseInt(parts[0], 10);
        const month = parseInt(parts[1], 10);
        const day = parseInt(parts[2], 10);
        if (
          isNaN(year) ||
          isNaN(month) ||
          isNaN(day) ||
          month < 1 ||
          month > 12 ||
          day < 1 ||
          day > 31 ||
          year < 1934 ||
          year > 2014
        ) {
          isValid = false;
          errorMessages.birthday =
            "Please enter a valid birthday (yyyy-mm-dd, between 1934 and 2014).";
        }
      }
    }

    if (!isValid) {
      $(".error-message").remove();
      $(".error-field").removeClass("error-field");

      for (const field in errorMessages) {
        const inputField = $("#" + field);
        const errorDiv = $("<div>")
          .addClass("error-message")
          .text(errorMessages[field]);
        inputField.parent().append(errorDiv);
      }

      return;
    }
    $(".error-message").remove();
    $(".error-field").removeClass("error-field");
    const student = { ...Object.fromEntries(formData), id: students.length };
    students.push(student);
    createStudent(student);
    hideStudentModal();
    sendStudentDataToServer(student);
  }

  function sendStudentDataToServer(data) {
    console.log("Data sent to server:", data);
    fetch("http://127.0.0.1:5500/students", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok.");
        }
        return response.json();
      })
      .then((jsonData) => {
        console.log("Server response:", jsonData);
      })
      .catch((error) => {
        console.error("Error sending data to server:", error);
      });
  }

  function createStudent(
    { id, group, name, surname, birthday, gender, active },
    replace
  ) {
    const tr = $("<tr>").attr("id", `student-${id}`);
    const checkbox = $("<input>").attr("type", "checkbox");
    const checkBoxTd = $("<td>").append(checkbox);
    tr.append(checkBoxTd);

    const cells = [group, `${name} ${surname}`, gender, birthday];

    for (let cellContent of cells) {
      const td = $("<td>").text(cellContent);
      tr.append(td);
    }

    const activeTd = $("<td>");
    const activeDiv = $("<div>").addClass("status");
    active && activeDiv.addClass("active");
    activeTd.append(activeDiv);
    tr.append(activeTd);

    const optionsTd = $("<td>");
    const editButton = $("<button>");
    const removeButton = $("<button>");
    editButton.on("click", () => onStudentEditModal(id));
    removeButton.on("click", () => confirmDeletion(id));

    const editButtonIcon = $("<i>")
      .addClass("fa fa-edit")
      .css("color", "white");
    const removeButtonIcon = $("<i>")
      .addClass("fa fa-remove")
      .css("color", "white");

    editButton.append(editButtonIcon);
    removeButton.append(removeButtonIcon);

    optionsTd.append(editButton, removeButton);
    tr.append(optionsTd);

    if (replace) {
      $(`#student-${id}`).replaceWith(tr);
    } else {
      studentsTable.find("tbody").append(tr);
    }
    return tr;
  }

  function confirmDeletion(id) {
    showConfirmationModal();
    const confirmDeletionButton = $("#confirm-deletion-btn");
    const cancelDeletionButton = $("#cancel-deletion-btn");
    const student = students.find((el) => el.id === id);
    $("#confirm-question").text(
      `Are you sure you want to delete ${student.name} ${student.surname}?`
    );

    confirmDeletionButton.on("click", () => {
      removeStudent(id);
      hideConfirmationModal();
    });
    cancelDeletionButton.on("click", hideConfirmationModal);
  }

  function removeStudent(id) {
    $(`#student-${id}`).remove();
    students = students.filter((el) => el.id !== id);
  }

  function onStudentEditModal(id) {
    showStudentModal("edit", id);
    const student = students.find((el) => el.id === id);
    if (!student) return;
    $("#group").val(student.group);
    $("#name").val(student.name);
    $("#surname").val(student.surname);
    $("#gender").val(student.gender);
    $("#birthday").val(student.birthday);
  }

  function hideStudentModal() {
    addStudentModalWrapper.addClass("hidden");
  }

  function showStudentModal(mode, id) {
    if (mode === "edit") {
      $("#modal-title").text("Edit student");
      addStudentForm.off("submit").on("submit", (e) => {
        e.preventDefault();
        onStudentEdit(e, id);
      });
    } else {
      $("#modal-title").text("Create student");
      addStudentForm.off("submit").on("submit", (e) => {
        e.preventDefault();
        onStudentCreate(e);
      });
    }
    addStudentModalWrapper.removeClass("hidden");
  }

  function hideConfirmationModal() {
    confirmDeletionModalWrapper.addClass("hidden");
  }

  function showConfirmationModal() {
    confirmDeletionModalWrapper.removeClass("hidden");
  }

  function handleClickOutside({ target }, element, callback) {
    if (element && $(target).closest(element).length === 0) {
      callback();
    }
  }
});
