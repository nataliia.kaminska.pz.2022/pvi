$(document).ready(function () {
  const addStudentModalWrapper = $("#add-student");
  const confirmDeletionModalWrapper = $("#confirm-deletion");
  const addStudentModal = addStudentModalWrapper.find(".modal");
  const confirmDeletionModal = confirmDeletionModalWrapper.find(".modal");

  const addStudentForm = $("#add-student-form");
  const addStudentButton = $("#add-student-btn");
  const closeModalButton = $("#close-modal-btn");
  const studentsTable = $("#students-table");

  const students = [
    {
      id: 0,
      group: "PZ-22",
      name: "Konstantyn",
      surname: "Vykhovanets",
      birthday: "2004-12-27",
      gender: "F",
      active: false,
    },
    {
      id: 1,
      group: "PZ-22",
      name: "Denys",
      surname: "Hudzenko",
      birthday: "2005-01-22",
      gender: "M",
      active: false,
    },
  ];

  students.forEach((el) => createStudent(el));

  addStudentButton.on("click", showStudentModal);
  closeModalButton.on("click", hideStudentModal);

  $(document).on("mousedown", function (e) {
    handleClickOutside(e, addStudentModal, hideStudentModal);
    handleClickOutside(e, confirmDeletionModal, hideConfirmationModal);
  });

  function onStudentEdit(e, id) {
    const formData = new FormData(e.target);
    const student = { ...Object.fromEntries(formData), id };
    students[students.findIndex((el) => el.id === id)] = student;
    sendStudentDataToServer(student).then((isValid) => {
      if (isValid) {
        students.push(student);
        createStudent(student);
      } else {
        alert("Something went wrong. Check console for more info.");
      }
      hideStudentModal();
    });
  }

  function onStudentCreate(e) {
    e.preventDefault();
    const formData = new FormData(e.target);

    const student = { ...Object.fromEntries(formData), id: students.length };
    sendStudentDataToServer(student).then((isValid) => {
      if (isValid) {
        students.push(student);
        createStudent(student);
      } else {
        alert("Something went wrong. Check console for more info.");
      }
      hideStudentModal();
    });
  }

  function sendStudentDataToServer(data) {
    console.log("Data sent to server:", data);
    return fetch("http://localhost/forpvi/validate_student.php", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok.");
        }
        return response.json();
      })
      .then((jsonData) => {
        if (jsonData.error) {
          for (const errorField in jsonData.errors) {
            console.error(`${errorField}: ${jsonData.errors[errorField]}`);
          }
          return false;
        } else {
          console.log("Server response:", jsonData);
          return true;
        }
      })
      .catch((error) => {
        console.error("Error sending data to server:", error);
        return false;
      });
  }

  function createStudent(
    { id, group, name, surname, birthday, gender, active },
    replace
  ) {
    const tr = $("<tr>").attr("id", `student-${id}`);
    const checkbox = $("<input>").attr("type", "checkbox");
    const checkBoxTd = $("<td>").append(checkbox);
    tr.append(checkBoxTd);

    const cells = [group, `${name} ${surname}`, gender, birthday];

    for (let cellContent of cells) {
      const td = $("<td>").text(cellContent);
      tr.append(td);
    }

    const activeTd = $("<td>");
    const activeDiv = $("<div>").addClass("status");
    active && activeDiv.addClass("active");
    activeTd.append(activeDiv);
    tr.append(activeTd);

    const optionsTd = $("<td>");
    const editButton = $("<button>");
    const removeButton = $("<button>");
    editButton.on("click", () => onStudentEditModal(id));
    removeButton.on("click", () => confirmDeletion(id));

    const editButtonIcon = $("<i>")
      .addClass("fa fa-edit")
      .css("color", "white");
    const removeButtonIcon = $("<i>")
      .addClass("fa fa-remove")
      .css("color", "white");

    editButton.append(editButtonIcon);
    removeButton.append(removeButtonIcon);

    optionsTd.append(editButton, removeButton);
    tr.append(optionsTd);

    if (replace) {
      $(`#student-${id}`).replaceWith(tr);
    } else {
      studentsTable.find("tbody").append(tr);
    }
    return tr;
  }

  function confirmDeletion(id) {
    showConfirmationModal();
    const confirmDeletionButton = $("#confirm-deletion-btn");
    const cancelDeletionButton = $("#cancel-deletion-btn");
    const student = students.find((el) => el.id === id);
    $("#confirm-question").text(
      `Are you sure you want to delete ${student.name} ${student.surname}?`
    );

    confirmDeletionButton.on("click", () => {
      removeStudent(id);
      hideConfirmationModal();
    });
    cancelDeletionButton.on("click", hideConfirmationModal);
  }

  function removeStudent(id) {
    $(`#student-${id}`).remove();
    students = students.filter((el) => el.id !== id);
  }

  function onStudentEditModal(id) {
    showStudentModal("edit", id);
    const student = students.find((el) => el.id === id);
    if (!student) return;
    $("#group").val(student.group);
    $("#name").val(student.name);
    $("#surname").val(student.surname);
    $("#gender").val(student.gender);
    $("#birthday").val(student.birthday);
  }

  function hideStudentModal() {
    addStudentModalWrapper.addClass("hidden");
  }

  function showStudentModal(mode, id) {
    if (mode === "edit") {
      $("#modal-title").text("Edit student");
      addStudentForm.off("submit").on("submit", (e) => {
        e.preventDefault();
        onStudentEdit(e, id);
      });
    } else {
      $("#modal-title").text("Create student");
      addStudentForm.off("submit").on("submit", (e) => {
        e.preventDefault();
        onStudentCreate(e);
      });
    }
    addStudentModalWrapper.removeClass("hidden");
  }

  function hideConfirmationModal() {
    confirmDeletionModalWrapper.addClass("hidden");
  }

  function showConfirmationModal() {
    confirmDeletionModalWrapper.removeClass("hidden");
  }

  function handleClickOutside({ target }, element, callback) {
    if (element && $(target).closest(element).length === 0) {
      callback();
    }
  }
});
