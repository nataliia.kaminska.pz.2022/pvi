<?php
/*
$data = json_decode(file_get_contents("php://input"), true);

$errors = [];
if (empty($data['group'])) {
    $errors['group'] = "Group must be selected.";
}
if (empty($data['name']) || !preg_match('/^[A-Za-z]+$/', $data['name'])) {
    $errors['name'] = "Name must contain only letters.";
}
/*if (empty($data['surname']) || !preg_match('/^[A-Za-z]+$/', $data['name'])) {
  $errors['surname'] = "Surname must contain only letters.";
}
if (empty($data['birthday']) || !preg_match('/^[A-Za-z]+$/', $data['name'])) {
  $errors['birthday'] = "Name must contain only letters.";
}
if (empty($data['birthday']) || !preg_match('/^\d{4}-\d{2}-\d{2}$/', $data['birthday'])) {
  $errors['birthday'] = "Please enter a valid birthday (yyyy-mm-dd).";
} else {
  $parts = explode('-', $data['birthday']);
  $year = intval($parts[0]);
  $month = intval($parts[1]);
  $day = intval($parts[2]);
  if ($month < 1 || $month > 12 || $day < 1 || $day > 31 || $year < 1934 || $year > 2014) {
      $errors['birthday'] = "Please enter a valid birthday (yyyy-mm-dd, between 1934 and 2014).";
  }
}*/
/*
if (!empty($errors)) {
    http_response_code(400); // Помилка валідації
    echo json_encode(['success' => false, 'errors' => $errors]);
} else {
    echo json_encode(['success' => true, 'message' => 'Student added successfully']);
}

*/
header('Content-Type: application/json');

// Validate data
$group = $_POST['group'] ?? '';
$name = $_POST['name'] ?? '';
$gender = $_POST['gender'] ?? '';
$birthday = $_POST['birthday'] ?? '';

$errors = [];
if (empty($group)) {
  $errors['group'] = 'Group is required.';
}
if (empty($name)) {
  $errors['name'] = 'Name is required.';
}
if (empty($gender)) {
  $errors['gender'] = 'Gender is required.';
}
if (empty($birthday)) {
  $errors['birthday'] = 'Birthday is required.';
}

if (!empty($errors)) {
  http_response_code(400);
  echo json_encode(['error' => true, 'message' => 'Validation failed.', 'errors' => $errors]);
  exit;
}

// If data is valid, return it
$response = ['error' => false, 'student' => $_POST];
echo json_encode($response);


?>
